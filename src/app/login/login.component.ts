import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { DataUserService } from '../shared/data-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm !: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private httpClient: HttpClient,
              private router: Router,
              private dataUserService: DataUserService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      pass: ['', Validators.required]
    })
  }

  login() {
    //Haciendo petición tipo GET a api
    this.httpClient.get<any>("http://localhost:3000/usuarios")
      .subscribe(res => {
        //Iterando sobre el arreglo de usuarios
        const user = res.find((a: any) => {
          //Comprobando si las credenciales son correctas
          return a.email === this.loginForm.value.email && a.pass === this.loginForm.value.pass
        });
        if(user) {  
          //Pasando los datos del usuario al servicio
          this.dataUserService.datosUsuario = user;
          alert("Credenciales Correctas");
          this.loginForm.reset();
          this.router.navigate(['dashboard']);
        } else {
          alert("Error en las credenciales, verifique usuario y / o contraseña");
        }
      }, error => {
        console.log(error);
        alert("Error en el servidor, intente más tarde");
        this.router.navigate(['login']);
      })
  }

  get f() {
    //Función para regresar los errores que arroja la validación
    return this.loginForm.controls;
  }
}
