import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  //Sección de Productos

  agregarProducto(producto: any) {
    return this.httpClient.post<any>('http://localhost:3000/productos', producto)
      .pipe(map((res: any) => {
        return res;
      }))
  }

  obtenerProductos() {
    return this.httpClient.get('http://localhost:3000/productos')
      .pipe(map((res: any) => {
        return res;
      }))
  }

  editarProducto(producto: any, id: string) {
    return this.httpClient.put<any>("http://localhost:3000/productos/"+id,producto)
      .pipe(map((res: any) => {
        return res;
      }))
  }

  eliminarProducto(id: string) {
    return this.httpClient.delete<any>("http://localhost:3000/productos/"+id)
      .pipe(map((res: any) => {
        return res;
      }))
  }

  //Sección Obtener Usuarios

  obtenerUsuarios() {
    return this.httpClient.get<any>("http://localhost:3000/usuarios/")
    .pipe(map((res:any) => {
      return res;
    }))
  }
}
