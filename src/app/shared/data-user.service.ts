import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataUserService {
  //Variable puente entre el componente de Inicio de Sesión y el Dashboard
  datosUsuario: any;

  constructor() { }
}
