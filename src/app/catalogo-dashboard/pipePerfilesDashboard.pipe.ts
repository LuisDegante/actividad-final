import { Pipe, PipeTransform } from "@angular/core";
import { CatalogoDashboardComponent } from './catalogo-dashboard.component';

@Pipe({
    name: 'CambioTextoDashboard'
})
export class PipePerfilesDashboard implements PipeTransform {
    constructor(private catalogoDashboardComponent: CatalogoDashboardComponent) {}

    transform(valor: string) {
        return valor === 'seller' ? 'Usuario Vendedor' : 'Usuario Administrador';
    }
}