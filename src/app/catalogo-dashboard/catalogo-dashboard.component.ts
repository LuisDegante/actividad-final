import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductoDashboard } from './producto-dashboard.model';
import { ApiService } from '../shared/api.service';
import { Router } from '@angular/router';
import { DataUserService } from '../shared/data-user.service';

@Component({
  selector: 'app-catalogo-dashboard',
  templateUrl: './catalogo-dashboard.component.html',
  styleUrls: ['./catalogo-dashboard.component.css']
})
export class CatalogoDashboardComponent implements OnInit {
  
  formValue !: FormGroup;
  productoModelObj : ProductoDashboard = new ProductoDashboard();
  productos !: any;
  mostrarAgregar !: boolean;
  mostrarActualizar !: boolean;
  dataUserLogueado: any;

  constructor(private formBuilder: FormBuilder,
              private apiService: ApiService,
              private router:Router,
              public dataUserService: DataUserService) { }

  ngOnInit(): void {
    //Creación y validación de formulario
    this.formValue = this.formBuilder.group({
      id: ['', Validators.required],
      nombre_producto: ['', Validators.required],
      urlImagen: ['', Validators.required],
      marca: ['', Validators.required],
      precio: ['', Validators.required],
      stock: ['', Validators.required],
      descripcion: ['', Validators.required]
    })
    //Función para mostrar productos después de editar
    this.mostrarProductos();
    //Asignar a dataUserLogueado el valor del servicio que se trajo de login
    this.dataUserLogueado = this.dataUserService.datosUsuario;
  }

  clickAgregarProducto() {
    this.formValue.reset();
    this.mostrarAgregar = true;
    this.mostrarActualizar = false;
  }

  agregarProducto() {
    //Obteniendo los datos ingresados por el usuario
    this.productoModelObj.id = this.formValue.value.id;
    this.productoModelObj.nombre_producto = this.formValue.value.nombre_producto;
    this.productoModelObj.urlImagen = this.formValue.value.urlImagen;
    this.productoModelObj.marca = this.formValue.value.marca;
    this.productoModelObj.precio = this.formValue.value.precio;
    this.productoModelObj.descripcion = this.formValue.value.descripcion;
    this.productoModelObj.stock = this.formValue.value.stock;

    //Consultando la api para obtener los productos de la BD
    this.apiService.agregarProducto(this.productoModelObj)
      .subscribe(res => {
        alert("Producto Agregado Exitosamente");
        //Cerrar modal automáticamente
        let ref = document.getElementById('cancel');
        ref?.click();
        this.formValue.reset();
        //Función para mostrar productos usado para después de agregar
        this.mostrarProductos();
      }, err => {
        console.log(err);
        alert("Hubo Un Error al Agregar el Producto");
      })
  }

  eliminarProducto(producto: any) {
    this.apiService.eliminarProducto(producto.id)
      .subscribe(res => {
        alert("Producto Eliminado Exitosamente");
        this.mostrarProductos();
      }, err => {
        console.log(err);
        alert("Hubo un Error al Eliminar Este Producto");
      })
  }

  onEdit(producto: any) {
    //Condición para mostrar botones en modal
    this.mostrarAgregar = false;
    this.mostrarActualizar = true;

    this.productoModelObj.id = producto.id;
    //Rellenando los datos del producto en modal de edición
    this.formValue.controls['id'].setValue(producto.id);
    this.formValue.controls['nombre_producto'].setValue(producto.nombre_producto);
    this.formValue.controls['urlImagen'].setValue(producto.urlImagen);
    this.formValue.controls['marca'].setValue(producto.marca);
    this.formValue.controls['precio'].setValue(producto.precio);
    this.formValue.controls['descripcion'].setValue(producto.descripcion);
    this.formValue.controls['stock'].setValue(producto.stock);
  }

  guardarCambios() {
    this.productoModelObj.id = this.formValue.value.id;
    this.productoModelObj.nombre_producto = this.formValue.value.nombre_producto;
    this.productoModelObj.urlImagen = this.formValue.value.urlImagen;
    this.productoModelObj.marca = this.formValue.value.marca;
    this.productoModelObj.precio = this.formValue.value.precio;
    this.productoModelObj.descripcion = this.formValue.value.descripcion;
    this.productoModelObj.stock = this.formValue.value.stock;

    //Consultando la API de editarProducto
    this.apiService.editarProducto(this.productoModelObj, this.productoModelObj.id)
      .subscribe(res => {
        alert("Cambios Realizados Existosamente");
        //Cerrar modal automáticamente
        let ref = document.getElementById('cancel');
        ref?.click();
        this.formValue.reset();
        this.mostrarProductos();
      }, error => {
        alert("No puedes modificar el ID del elemento");
      })
  }

  mostrarProductos() {
    //Consultando API para obtener productos
    this.apiService.obtenerProductos()
      .subscribe(res => {
        this.productos = res;
      })
  }

  logout() {
    alert("Cerrando Sesión...");
    
    //Limpiando los datos del usuario logueado
    this.dataUserService.datosUsuario = '';
    this.router.navigate(['/login']);
  }
}