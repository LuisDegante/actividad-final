export class ProductoDashboard {
    id: string = '';
    nombre_producto: string = '';
    urlImagen: string = '';
    marca: string = '';
    precio: number = 0;
    stock: number = 0;
    descripcion: string = '';
}