import { Pipe, PipeTransform } from "@angular/core";
import { SignupComponent } from './signup.component';

@Pipe({
    name: 'CambioTexto'
})
export class PipePerfiles implements PipeTransform {
    constructor(private signupComponent: SignupComponent) {}

    transform(valor: string) {
        return valor === 'seller' ? 'Usuario Vendedor' : 'Usuario Administrador';
    }
}