import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public signupForm !: FormGroup;
  selectedPerfil: string = '';
  //Arreglo de perfiles disponibles en la BD
  perfiles: any = [
    'seller',
    'superAdmin'
  ]
  constructor(private formBuilder: FormBuilder,
              private httpClient: HttpClient,
              private router: Router) { }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      usuario: ['', Validators.required],
      email: ['', Validators.required],
      pass: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      perfil: ['', Validators.required]
    })
  }

  signup() {
    //Consultando API y almacenando datos en la BD
    this.httpClient.post<any>("http://localhost:3000/usuarios", this.signupForm.value)
      .subscribe(res => {
        alert("Usuario Creado Exitosamente");
        this.signupForm.reset();
        this.router.navigate(['login']);
      }, error => {
        console.log(error);
        alert("Hubo un error al crear al usuario");
      })
  }

  get f() {
    return this.signupForm.controls;
  }
}
