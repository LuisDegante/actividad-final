import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatalogoDashboardComponent } from './catalogo-dashboard/catalogo-dashboard.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { PipePerfiles } from './signup/pipePerfiles.pipe';
import { PipePerfilesDashboard } from './catalogo-dashboard/pipePerfilesDashboard.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CatalogoDashboardComponent,
    LoginComponent,
    SignupComponent,
    PipePerfiles,
    PipePerfilesDashboard
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
